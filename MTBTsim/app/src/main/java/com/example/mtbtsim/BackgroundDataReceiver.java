package com.example.mtbtsim;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;

public class BackgroundDataReceiver extends Thread {

    private Handler manager;
    private BluetoothSocket socket;
    private String msg;
    //BufferedReader bufferedReader;
    int app[];

    public BackgroundDataReceiver(Handler manager_par, BluetoothSocket socket_par)
    {
        manager=manager_par;
        socket=socket_par;
        msg="";
        app= new int[]{0, 0, 0};
    }

    public void run()
    {
        try {
            InputStream inputStream = socket.getInputStream();  //to obtain the bluetooth connection's input stream
            //bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
            int app;
            while(true)
            {
                if(Controllo(inputStream.read(),115))
                {
                    do
                    {
                        app=inputStream.read();
                        if(!Controllo(app,101))
                        {
                            msg+=(char)app;
                        }
                        else
                        {
                            sendMsg(msg);
                            msg="";
                        }
                    }while(!Controllo(app,101));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private boolean Controllo(int Byte_par,int controlType_par)
    {
        return Byte_par==controlType_par;
    }

    private void sendMsg(String msg_par)
    {
        Message json_str= manager.obtainMessage();  //obtain a message from the global message pool of the handler
        Bundle json_str_b = new Bundle();
        json_str_b.putString("refresh",msg_par);
        json_str.setData(json_str_b);
        manager.sendMessage(json_str);  //send the message to the handler that will operate with it by the handleMessage method
    }
}

