package com.example.mtbtsim;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    BluetoothSocket connection;
    JSONObject data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BluetoothAdapter bAdapter= BluetoothAdapter.getDefaultAdapter();
        final TextView T_Ain1 = findViewById(R.id.T_Ain1);
        final TextView T_Ain2 = findViewById(R.id.T_Ain2);
        final TextView T_Ain3 = findViewById(R.id.T_Ain3);
        final TextView T_Ain4 = findViewById(R.id.T_Ain4);
        final TextView T_Ain5 = findViewById(R.id.T_Ain5);
        final TextView T_Ain6 = findViewById(R.id.T_Ain6);
        final TextView T_Din1 = findViewById(R.id.T_Din1);
        final TextView T_Din2 = findViewById(R.id.T_Din2);
        final TextView T_Din3 = findViewById(R.id.T_Din3);
        final TextView T_Din4 = findViewById(R.id.T_Din4);
        final TextView T_Din5 = findViewById(R.id.T_Din5);
        final TextView T_Din6 = findViewById(R.id.T_Din6);
        final TextView T_Din7 = findViewById(R.id.T_Din7);
        final TextView T_Din8 = findViewById(R.id.T_Din8);
        final TextView T_Din9 = findViewById(R.id.T_Din9);
        final TextView T_Din10 = findViewById(R.id.T_Din10);
        final TextView T_Din11 = findViewById(R.id.T_Din11);
        final TextView T_Din12 = findViewById(R.id.T_Din12);
        final TextView T_Din13 = findViewById(R.id.T_Din13);
        final TextView T_Din14 = findViewById(R.id.T_Din14);
        final TextView T_Din15 = findViewById(R.id.T_Din15);
        final TextView T_out1 = findViewById(R.id.T_out1);
        final TextView T_out2 = findViewById(R.id.T_out2);
        final TextView T_out3 = findViewById(R.id.T_out3);
        final TextView T_out4 = findViewById(R.id.T_out4);
        final TextView T_out5 = findViewById(R.id.T_out5);
        final TextView T_out6 = findViewById(R.id.T_out6);

        if(!bAdapter.isEnabled()) //check if the bluetooth is enabled on the user's device
        {
            bAdapter.enable();
        }
        Set<BluetoothDevice> paired_device= bAdapter.getBondedDevices();  //list of all the devices that paired with the user's device at least one time
        bAdapter.cancelDiscovery();
        for(BluetoothDevice app: paired_device)
        {
            if(app.getName().equals("MiteBox2"))  //"MiteBox2" is ours Mite Box's name. It has to be changed with a parameter
            {
                try {
                    connection= app.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));  //initialize the connection with the Mite Box
                    if(connection!=null)
                    {
                        connection.connect();  //establish the connection with the Mite Box

                        //the following lines override the handleMessage(Message) standard method of the Handler class
                        Handler manager= new Handler()
                        {
                            @Override
                            public void handleMessage(Message msg_par)
                            {
                                Bundle buffer=msg_par.getData();
                                if(buffer.containsKey("refresh"))
                                {
                                    try {
                                        data =new JSONObject(buffer.getString("refresh"));
                                        T_Ain1.setText((String) data.get("Ain1"));
                                        T_Ain2.setText((String) data.get("Ain2"));
                                        T_Ain3.setText((String) data.get("Ain3"));
                                        T_Ain4.setText((String) data.get("Ain4"));
                                        T_Ain5.setText((String) data.get("Ain5"));
                                        T_Ain6.setText((String) data.get("Ain6"));
                                        T_Din1.setText((String) data.get("Din1"));
                                        T_Din2.setText((String) data.get("Din2"));
                                        T_Din3.setText((String) data.get("Din3"));
                                        T_Din4.setText((String) data.get("Din4"));
                                        T_Din5.setText((String) data.get("Din5"));
                                        T_Din6.setText((String) data.get("Din6"));
                                        T_Din7.setText((String) data.get("Din7"));
                                        T_Din8.setText((String) data.get("Din8"));
                                        T_Din9.setText((String) data.get("Din9"));
                                        T_Din10.setText((String) data.get("Din10"));
                                        T_Din11.setText((String) data.get("Din11"));
                                        T_Din12.setText((String) data.get("Din12"));
                                        T_Din13.setText((String) data.get("Din13"));
                                        T_Din14.setText((String) data.get("Din14"));
                                        T_Din15.setText((String) data.get("Din15"));
                                        T_out1.setText((String) data.get("out1"));
                                        T_out2.setText((String) data.get("out2"));
                                        T_out3.setText((String) data.get("out3"));
                                        T_out4.setText((String) data.get("out4"));
                                        T_out5.setText((String) data.get("out5"));
                                        T_out6.setText((String) data.get("out6"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        };
                        BackgroundDataReceiver dataReceiver_thr=new BackgroundDataReceiver(manager,connection);
                        dataReceiver_thr.start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
